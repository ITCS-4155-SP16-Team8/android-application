package g8.teamworkandroid4155;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import g8.teamworkandroid4155.parseobjects.Project;
import g8.teamworkandroid4155.parseobjects.Task;

/**
 * Description of TaskExpandableListAdapter.java
 *
 * @author Wesley Hager
 */
public class TaskExpandableListAdapter extends BaseExpandableListAdapter implements View.OnClickListener, Filterable {
    private Context context;
    private List<Task> displayTaskList;
    private List<Task> originalTaskList;
    private PopupMenu mUserPopup;
    private FragmentManager fragmentManager;
    private Project mProject;
    private TaskExpandableListFragment taskExpandableListFragment;
    private onFragmentEditTask mListener;

    public TaskExpandableListAdapter(Context context, List<Task> objects, FragmentManager childFragmentManager, Project project, TaskExpandableListFragment taskExpandableListFragment) {
        this.context = context;
        this.displayTaskList = objects;
        fragmentManager = childFragmentManager;
        mProject = project;
        this.taskExpandableListFragment = taskExpandableListFragment;

        if (context instanceof onFragmentEditTask) {
            mListener = (onFragmentEditTask) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement onFragmentEditTask");
        }
    }

    public interface onFragmentEditTask {
        void editTask(Task task);
    }

    public void replaceOriginalList(List<Task> tasks) {
        originalTaskList = new ArrayList<>(tasks);
    }

    @Override
    public int getGroupCount() {
        return this.displayTaskList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.displayTaskList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.displayTaskList.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell_tasks_expandable, parent, false);
        }
        TextView title = (TextView) convertView.findViewById(R.id.title_cell_task);
        TextView assignedTo = (TextView) convertView.findViewById(R.id.created_by_cell);
        TextView ratingLabel = (TextView) convertView.findViewById(R.id.rating_txt_Cell_Task);
        TextView rating = (TextView) convertView.findViewById(R.id.taskRating_cell_Task);
        TextView deadline = (TextView) convertView.findViewById(R.id.deadline_cell_task);
        Button reviewButton = (Button) convertView.findViewById(R.id.reviewButton_cell_task);

        Task taskPar = displayTaskList.get(groupPosition);
        Date date = taskPar.getTaskDeadline();
        ParseUser user = taskPar.getCreatedBy();

        title.setText(taskPar.getTaskName());
        assignedTo.setText(taskPar.getAssignedTo());
        assignedTo.setTag(groupPosition);
        assignedTo.setOnClickListener(this);

        if (taskPar.getDone()) {
            rating.setVisibility(View.VISIBLE);
            rating.setText(taskPar.getTaskRating());
            ratingLabel.setVisibility(View.VISIBLE);
        } else {
            rating.setVisibility(View.GONE);
            rating.setText("");
            ratingLabel.setVisibility(View.GONE);
        }

        if (taskPar.getDone() && !taskPar.getHasRated(ParseUser.getCurrentUser()) && !taskPar.getAssignedToID().equals(ParseUser.getCurrentUser().getObjectId())) {
            reviewButton.setVisibility(View.VISIBLE);
            reviewButton.setTag(taskPar);
            reviewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TaskRatingDialogFragment dialogFragment = new TaskRatingDialogFragment();
                    dialogFragment.putTask((Task) v.getTag(), groupPosition);
                    dialogFragment.show(fragmentManager, "Event");
                    notifyDataSetChanged();
                }
            });
        } else {
            reviewButton.setVisibility(View.GONE);
        }

        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(date.getTime());
            deadline.setText(DateFormat.format(Application.TIMESTAMP_FORMAT, cal));

            if (cal.before(Calendar.getInstance()) && !taskPar.getDone()) {
                deadline.setTextColor(context.getResources().getColor(R.color.deadlineLATE));
            } else {
                deadline.setTextColor(context.getResources().getColor(R.color.deadlineEARLY));
            }
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell_child_title_content, parent, false);
        }

        Button editButton = (Button) convertView.findViewById(R.id.editTask_Button_ChildCell);
        TextView title = (TextView) convertView.findViewById(R.id.title_ChildCell);
        TextView detail = (TextView) convertView.findViewById(R.id.detail_ChildCell);

        editButton.setTag(groupPosition);
        editButton.setOnClickListener(this);

        Task task = displayTaskList.get(groupPosition);
        if (childPosition == 0) {
            title.setText("Description:");
            detail.setText(task.getTaskDescription());
            if (task.getDone() || !task.getAssignedToID().equals(ParseUser.getCurrentUser().getObjectId()))
                editButton.setVisibility(View.GONE);
            else
                editButton.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editTask_Button_ChildCell:
                int taskPos = Integer.parseInt(v.getTag().toString());
                Task t = displayTaskList.get(taskPos);
                mListener.editTask(t);
                break;
            case R.id.created_by_cell:
                clickedAssignedUser(v);
                break;
        }
    }

    private void clickedAssignedUser(final View v) {
        final int taskPos = Integer.parseInt(v.getTag().toString());
        final Task t = displayTaskList.get(taskPos);
        String userID = ParseUser.getCurrentUser().getObjectId();

        if (!t.getDone() && (t.getAssignedToID().equals(userID) || t.getAssignedToID().equals(""))) {
            mUserPopup = new PopupMenu(context, v);
            Menu menu = mUserPopup.getMenu();
            menu.clear();
            menu.add(1, 0, 0, "(Unassigned)");

            for (int i = 1; i <= taskExpandableListFragment.getUsers().size(); i++)
                menu.add(1, i, i, taskExpandableListFragment.getUsers().get(i - 1).getUsername());

            mUserPopup.show();

            mUserPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {

                    int itemPos = item.getItemId();

                    Task t = displayTaskList.get(taskPos);

                    if (itemPos > 0) {
                        t.setAssignedTo(taskExpandableListFragment.getUsers().get(itemPos - 1));
                        mProject.createNewMessage(t.getTaskName() + context.getString(R.string.msgAssignTo) + taskExpandableListFragment.getUsers().get(itemPos - 1).getUsername());
                    } else {
                        t.setAssignedToNothing();
                        mProject.createNewMessage(t.getTaskName() + context.getString(R.string.msgUnassigned));
                    }
                    t.saveInBackground();
                    notifyDataSetChanged();
                    return false;
                }
            });
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<Task> filtered = new ArrayList<>();
                Log.d("inside", "filtered");
                Date checkDate = new Date();
                String currentUser = ParseUser.getCurrentUser().getUsername();

                if (originalTaskList != null)
                    if (constraint == null || constraint.toString().equals("All")) {
                        filtered.addAll(originalTaskList);
                    } else if (constraint.toString().equals("In Progress")) {
                        Log.d("inside", "its in progress");
                        for (Task t : originalTaskList) {
                            if (!t.getDone())
                                filtered.add(t);
                        }
                    } else if (constraint.toString().equals("Completed")) {
                        for (Task t : originalTaskList) {
                            if (t.getDone())
                                filtered.add(t);
                        }
                    } else if (constraint.toString().equals("Personal")) {
                        for (Task t : originalTaskList) {
                            if (t.getAssignedTo().equals(currentUser))
                                filtered.add(t);
                        }
                    } else if (constraint.toString().equals("Overdue")) {
                        for (Task t : originalTaskList) {
                            if (t.getTaskDeadline().before(checkDate) && !t.getDone())
                                filtered.add(t);
                        }
                    }

                results.count = filtered.size();
                results.values = filtered;

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                displayTaskList.clear();
                notifyDataSetChanged();
                if (results.count > 0)
                    displayTaskList.addAll(((ArrayList<Task>) results.values));
                notifyDataSetInvalidated();
            }
        };
    }
}