package g8.teamworkandroid4155;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import g8.teamworkandroid4155.parseobjects.Notification;

public class NotificationDialogListAdapter extends ArrayAdapter<Notification> {
    private List<Notification> notificationList;
    private Context mContext;
    private int mResource;

    public NotificationDialogListAdapter(Context context, int resource, List<Notification> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        this.notificationList = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);
        }

        final Notification notification = notificationList.get(position);

        TextView nContent = (TextView) convertView.findViewById(R.id.notificationContent_TV_cellNotify);
        CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkBox_cellNotify);

        nContent.setText(notification.getNotificationMsg());

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notification.setHasBeenRead(true);
                notification.saveInBackground();
                remove(notification);
            }
        });

        return convertView;
    }
}
