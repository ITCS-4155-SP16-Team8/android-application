package g8.teamworkandroid4155;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.parse.ParseUser;

import g8.teamworkandroid4155.parseobjects.Task;

/**
 * Description of TaskRatingDialogFragment.java
 * Displays fragment with a textView and slider for rating a Task
 *
 * @author Wesley Hager
 */
public class TaskRatingDialogFragment extends DialogFragment {
    private TextView mTitle;
    private SeekBar mRating;
    private Task task;
    private int position;
    private taskRatedListener mListener;

    public void putTask(Task t, int p) {
        task = t;
        position = p;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof taskRatedListener) {
            mListener = (taskRatedListener) activity;
        } else {
            throw new RuntimeException(activity.toString() + " must implement taskRatedListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Inflate fragment_dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.fragment_dialog, null);

        mTitle = (TextView) v.findViewById(R.id.taskRating_txt_Rating);
        mRating = (SeekBar) v.findViewById(R.id.taskRating_seekBar_Rating);

        mRating.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTitle.setText(getResources().getTextArray(R.array.rank_participation)[progress]);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        builder.setView(v)
                .setMessage("Task Name: " + task.getTaskName() + "\nAssigned: " + task.getAssignedTo())
                .setCancelable(true)
                .setPositiveButton(R.string.taskRate, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, int id) {
                        task.setHasRated(ParseUser.getCurrentUser(), mRating.getProgress() + 1);
                        task.saveInBackground();
                        mListener.ratedTask(position);
                    }
                })
                .setNegativeButton(R.string.cancel, null);

        return builder.create();
    }

    public interface taskRatedListener {
        void ratedTask(int pos);
    }
}