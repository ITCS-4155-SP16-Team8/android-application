package g8.teamworkandroid4155.parseobjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseRole;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

/**
 * Description of Project.java
 *
 * @author Wesley Hager
 */
@ParseClassName("Project")
public class Project extends ParseObject implements Serializable {

    public static final String COMMUNICATION_STAT = "communication";
    public static final String PARTICIPATION_STAT = "participation";
    public static final String HELPFULNESS_STAT = "helpfulness";

    public static ParseQuery<Project> getQueryWithRoleUsers() {
        return ParseQuery.getQuery(Project.class).include("projectRole").include("projectRole").orderByAscending("projectName");
    }

    public String getProjectName() {
        return getString("projectName");
    }

    public void setProjectName(String pName) {
        put("projectName", pName);
    }

    public String getProjectDescription() {
        return (getString("projectDescription") != null) ? getString("projectDescription") : "(Empty)";
    }

    public void setProjectDescription(String pDesc) {
        put("projectDescription", pDesc);
    }

    public Date getProjectDeadline() {
        return getDate("projectDeadline");
    }

    public void setProjectDeadline(Date pDate) {
        put("projectDeadline", pDate);
    }

    public ParseRelation<Task> getProjectRelationToTask() {
        return getRelation("projectRelationTask");
    }

    public ParseRelation getProjectRelationToMessages() {
        return getRelation("projectRelationMessage");
    }

    public void setRolePointer(ParseRole role) {
        put("projectRole", role);
    }

    public void setProjectDone() {
        put("done", true);
    }

    public boolean getProjectDone() {
        return getBoolean("done");
    }

    /**
     * Creates rating for a ParseUser
     *
     * @param rater         ParseUser ObjectId that is rating others
     * @param rated         ParseUser ObjectId that is being rated
     * @param communication Stat 1..5
     * @param helpfulness   Stat 1..5
     * @param participation Stat 1..5
     * @return JSONObject as an Object
     */
    private Object createJsonObject(String rater, String rated, int communication, int helpfulness, int participation) {
        JSONObject m = new JSONObject();

        try {
            m.put("communication", communication);
            m.put("helpfulness", helpfulness);
            m.put("participation", participation);
            m.put("ratee", rated);
            m.put("rater", rater);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return m;
    }

    /**
     * Determines if a user has been rated
     *
     * @param ratedUser ParseUser that was rated
     * @return false = not rated, true = has rated
     */
    public boolean getHasPeerReviewed(ParseUser ratedUser) {
        JSONArray mArray = getJSONArray("peerReview");
        if (mArray == null) {
            return false;
        } else {
            for (int i = 0; i < mArray.length(); i++) {
                try {
                    if ((mArray.getJSONObject(i).getString("ratee")).equals(ratedUser.getObjectId())
                            && (mArray.getJSONObject(i).getString("rater")).equals(ParseUser.getCurrentUser().getObjectId())) {
                        return true;
                    }
                } catch (JSONException ignored) {
                }
            }
        }
        return false;
    }

    /**
     * Checks where a user has already been rated
     *
     * @param ratedUser ParseUser
     * @return position in JSONArray else -1
     */
    private int getHasPeerReviewedPOS(ParseUser ratedUser) {
        JSONArray mArray = getJSONArray("peerReview");
        if (mArray == null) {
            return 0;
        } else {
            for (int i = 0; i < mArray.length(); i++) {
                try {
                    if ((mArray.getJSONObject(i).getString("ratee")).equals(ratedUser.getObjectId())
                            && (mArray.getJSONObject(i).getString("rater")).equals(ParseUser.getCurrentUser().getObjectId())) {
                        return i;
                    }
                } catch (JSONException ignored) {
                }
            }
        }
        return -1;
    }

    /**
     * Sets a project rating for user, attempts to get json array
     * if a rating exists then overwrite position in array. {@link #getHasPeerReviewedPOS(ParseUser)}
     *
     * @param ratedUser User that is being rated
     * @param comm      Communication Stat
     * @param help      Helpfulness Stat
     * @param part      Participation Stat
     * @see #createJsonObject(String, String, int, int, int)
     */
    public void setHasPeerReviewed(ParseUser ratedUser, int comm, int help, int part) {
        JSONArray mArray = getJSONArray("peerReview");
        if (mArray == null) {
            mArray = new JSONArray();
        }

        int pos;
        if ((pos = getHasPeerReviewedPOS(ratedUser)) >= 0) {
            try {
                mArray.put(pos, createJsonObject(ParseUser.getCurrentUser().getObjectId(), ratedUser.getObjectId(), comm, help, part));
            } catch (JSONException ignored) {
            }
        } else {
            mArray.put(createJsonObject(ParseUser.getCurrentUser().getObjectId(), ratedUser.getObjectId(), comm, help, part));
        }

        put("peerReview", mArray);

    }

    /**
     * Create a message for project with current ACL
     *
     * @param content Content of the message
     */
    public void createNewMessage(String content) {
        Message msg = new Message();
        msg.setMessageContent(content);
        msg.setMessagePostedBy(ParseUser.getCurrentUser());
        msg.setProject(Project.this);
        msg.setACL(getACL());
        msg.saveInBackground();
    }

    /**
     * Returns the value of stat for the given user
     *
     * @param parseUser a member of the project
     * @param stat      The String constant for a stat
     * @return a double from 0 to 5
     */
    public double getMemberStat(ParseUser parseUser, String stat) {
        JSONArray mArray = getJSONArray("peerReview");
        if (mArray == null) {
            return 0;
        } else {
            double total = 0;
            int count = 0;
            for (int i = 0; i < mArray.length(); i++) {
                try {
                    JSONObject object = mArray.getJSONObject(i);
                    if ((object.getString("ratee")).equals(parseUser.getObjectId())) {
                        total += object.getDouble(stat);
                        count++;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return total / count;
        }
    }

}
