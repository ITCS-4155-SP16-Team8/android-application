package g8.teamworkandroid4155.createactivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseRole;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Calendar;
import java.util.UUID;

import g8.teamworkandroid4155.MainActivity;
import g8.teamworkandroid4155.R;
import g8.teamworkandroid4155.parseobjects.Project;

/**
 * Description of CreateProjectActivity.java
 *
 * @author Wesley Hager
 */
public class CreateProjectActivity extends Fragment {

    private EditText pName, pDate, pDescription;
    private Button saveProject;
    private Calendar date = Calendar.getInstance();
    private Project pProject;
    private DatePickerDialog datePicker;
    private ParseRole newProjectRole;
    private OnFragmentInteractionListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_create_project, container, false);

        saveProject = (Button) view.findViewById(R.id.save_button_createProject);
        pName = (EditText) view.findViewById(R.id.projectName_createProject);
        pDate = (EditText) view.findViewById(R.id.projectDate_createProject);
        pDescription = (EditText) view.findViewById(R.id.projectDescription_createProject);

        saveProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createProject();
            }
        });
        pDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDatePicker();
            }
        });

        pProject = new Project();

        return view;
    }

    // Create Role Then create Project
    private void createProject() {

        // Create ACL for the new Role
        ParseACL roleACL = new ParseACL();
        roleACL.setReadAccess(ParseUser.getCurrentUser(), true);
        roleACL.setWriteAccess(ParseUser.getCurrentUser(), true);

        Long uuid = UUID.randomUUID().getMostSignificantBits();
        newProjectRole = new ParseRole(Long.toHexString(uuid), roleACL);
        // Must add CurrentUser to inherit the permissions
        newProjectRole.getUsers().add(ParseUser.getCurrentUser());
        newProjectRole.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    createTheProject();
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    private void createTheProject() {

        ParseACL newACL = new ParseACL();
        newACL.setRoleReadAccess(newProjectRole, true);
        newACL.setRoleWriteAccess(newProjectRole, true);
//        newACL.setReadAccess(ParseUser.getCurrentUser(), true);
//        newACL.setWriteAccess(ParseUser.getCurrentUser(), true);

        pProject.setProjectName(pName.getText().toString());
        pProject.setProjectDeadline(date.getTime());
        pProject.setRolePointer(newProjectRole);
        pProject.setProjectDescription(pDescription.getText().toString());
        pProject.setACL(newACL);

        pProject.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    pProject.createNewMessage(pProject.getProjectName() + getString(R.string.projectCreatedMessage));
                    Toast.makeText(getContext(), "Created New Project", Toast.LENGTH_SHORT).show();
                    mListener.onFragmentInteraction(MainActivity.LOAD_USERMAIN);
                } else {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Failed to make project", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadDatePicker() {
        datePicker = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                pDate.setText(String.format("%d/%02d/%d", monthOfYear + 1, dayOfMonth, year));
                date.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
            }
        }, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH));
        datePicker.show();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int pos);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
