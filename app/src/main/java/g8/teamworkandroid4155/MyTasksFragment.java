package g8.teamworkandroid4155;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import g8.teamworkandroid4155.parseobjects.Project;
import g8.teamworkandroid4155.parseobjects.Task;

/**
 * Description of MyTasksFragment.java
 *
 * @author Wesley Hager
 */
public class MyTasksFragment extends Fragment {

    private ListView tasksListView;
    private List<Task> tasks;
    private MyTasksAdapter myTasksAdapter;
    private PullRefreshLayout pullRefreshLayout;
    private ParseQuery<Task> tQuery;
    private TextView notification;
    private OnFragmentInteractionListener mListener;
    private Task task;


    public MyTasksFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        tasks = new ArrayList<>();
        View view = inflater.inflate(R.layout.fragment_my_tasks, container, false);
        tasksListView = (ListView) view.findViewById(R.id.listview_myTask);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.swipeRefreshLayout_myTask);
        notification = (TextView) view.findViewById(R.id.taskNotification_myTask);

        myTasksAdapter = new MyTasksAdapter(getContext(), R.layout.cell_my_task, tasks);
        myTasksAdapter.setNotifyOnChange(true);
        tasksListView.setAdapter(myTasksAdapter);

        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                findMyTasks();
            }
        });

        tasksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("myTaskFragment", position + "");
                mListener.onFragmentInteraction(tasks.get(position).getProject());
            }
        });

        findMyTasks();
        return view;
    }

    public void findMyTasks() {
        pullRefreshLayout.setRefreshing(true);
        if (tQuery == null) {
            tQuery = ParseQuery.getQuery("Task");
            tQuery.include("projectPointer");
            tQuery.include("createdBy");
            tQuery.whereEqualTo("assignedTo", ParseUser.getCurrentUser());
            tQuery.orderByAscending("done");
            tQuery.addAscendingOrder("projectPointer");
            tQuery.addAscendingOrder("TaskDeadline");
        }
        tQuery.findInBackground(new FindCallback<Task>() {
            @Override
            public void done(List<Task> list, ParseException e) {
                pullRefreshLayout.setRefreshing(false);
                if (e == null) {
                    loadList(list);
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    private void loadList(List<Task> task) {
        if (task.size() == 0) {
            notification.setVisibility(View.VISIBLE);
        } else {
            notification.setVisibility(View.GONE);
            myTasksAdapter.clear();
            myTasksAdapter.addAll(task);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Project project);
    }
}
