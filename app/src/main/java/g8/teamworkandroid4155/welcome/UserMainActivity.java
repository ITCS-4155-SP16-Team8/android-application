package g8.teamworkandroid4155.welcome;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import g8.teamworkandroid4155.MyTasksFragment;
import g8.teamworkandroid4155.R;

public class UserMainActivity extends Fragment {

    private UserMainSectionAdapter mAdapter;
    private ViewPager viewPager;
    private Bundle bun;
    private OnFragmentInteractionListener mListener;
    private MyTasksFragment myTasksFragment;
    private CurrentUserFragment currentUserFragment;


    public UserMainActivity() { // Requires Empty Constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_user_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        viewPager = (ViewPager) view.findViewById(R.id.userContainer);

        mAdapter = new UserMainSectionAdapter(getChildFragmentManager());

        viewPager.setAdapter(mAdapter);
        if (viewPager != null) {
            viewPager.setAdapter(mAdapter);
        }

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.userTabBar);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
        }

    }

    public void updateCurrentUserFragmentBitmap(Bitmap img){
        currentUserFragment.updateProfileImage(img);
    }


    //    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.logout_Menu) {
//            ParseUser.logOutInBackground(new LogOutCallback() {
//                @Override
//                public void done(ParseException e) {
//                    startActivity(new Intent(UserMainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
//                }
//            });
//        }else if (item.getItemId() == R.id.addProject_Menu){
//            startActivityForResult(new Intent(UserMainActivity.this, CreateProjectActivity.class), Application.SAVED_NEW_PROJECT);
//        }
//        return true;
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int pos);
    }

    public class UserMainSectionAdapter extends FragmentPagerAdapter {

        public UserMainSectionAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                myTasksFragment = new MyTasksFragment();
                return myTasksFragment;
            }
            if (position == 1) {
                currentUserFragment = new CurrentUserFragment();
                return currentUserFragment;
            }

            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "My Tasks";
                case 1:
                    return getString(R.string.userHome);
            }
            return null;
        }
    }
}
