package g8.teamworkandroid4155.parseobjects;

import com.parse.ParseFile;
import com.parse.ParseUser;

import java.io.Serializable;

/**
 * Description of ParUser.java
 * Provides getters for user stats
 *
 * @author Wesley Hager
 */
public class ParUser extends ParseUser implements Serializable {

    /**
     * @return this ParseUser total stats: [(sum of stats) / 3]
     * @see #getHelpfulnessStat()
     * @see #getCommunicationStat()
     * @see #getParticipationStat()
     */
    public double getTotalStat() {
        return (getHelpfulnessStat() + getCommunicationStat() + getParticipationStat()) / 3;
    }

    /**
     * @return this ParseUser helpfulness stat
     */
    public double getHelpfulnessStat() {
        return getDouble("helpfulnessStat");
    }

    /**
     * @return this ParseUser communication stat
     */
    public double getCommunicationStat() {
        return getDouble("communicationStat");
    }

    /**
     * @return this ParseUser participation stat
     */
    public double getParticipationStat() {
        return getDouble("participationStat");
    }

    /**
     * @param file Profile Image
     */
    public void putProfileImage(ParseFile file) {
        put("picture", file);
    }
}
