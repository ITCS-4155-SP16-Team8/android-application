package g8.teamworkandroid4155.welcome;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.List;

import g8.teamworkandroid4155.Application;
import g8.teamworkandroid4155.ProjectTabActivity;
import g8.teamworkandroid4155.R;
import g8.teamworkandroid4155.parseobjects.Project;

public class ProjectsFragment extends Fragment implements FindCallback<Project>, AdapterView.OnItemClickListener {

    private ProgressDialog prog;
    private Context mContext;
    private ListView projectsListView;
    private List<Project> projects;
    private ProjectsListAdapter adapter;
    private OnFragmentInteractionListener mListener;
    public ProjectsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mContext = getContext();

        View view = inflater.inflate(R.layout.fragment_project, container, false);
        projectsListView = (ListView) view.findViewById(R.id.projects_listView_Projects);

        projects = new ArrayList<>();
        adapter = new ProjectsListAdapter(this.mContext, R.layout.cell_projects, projects);

        projectsListView.setAdapter(adapter);
        projectsListView.setOnItemClickListener(this);

        prog = new ProgressDialog(this.mContext);
        prog.setTitle("Loading Projects");
        prog.setCancelable(false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        // MARK: -  Reload Projects
        findProjects();
    }

    private void findProjects() {
       // prog.show();
//        ParseQuery<Project> pQuery = Project.getQueryWithRoleUsers();
//        pQuery.findInBackground(ProjectsFragment.this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("itemSelect", "hello " + position);

//        mProject.setCurrentProject(projects.get(position));
        startActivity(new Intent(mContext, ProjectTabActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void done(List<Project> list, ParseException e) {
        prog.dismiss();

        if (e == null) {
            for (Project p : list) {
                Log.d("Projects", p.toString() + " Project Name: " + p.getProjectName());
            }
            projects = list;
            loadList(projects);
        } else {
            // Todo: Remove e.printStackTrace();
            e.printStackTrace();
        }
    }

    private void loadList(List<Project> projects) {
        adapter.clear();
        adapter.addAll(projects);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Application.SAVED_NEW_PROJECT) {
            findProjects();
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
