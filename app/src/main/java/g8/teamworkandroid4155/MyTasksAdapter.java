package g8.teamworkandroid4155;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.SaveCallback;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import g8.teamworkandroid4155.parseobjects.Project;
import g8.teamworkandroid4155.parseobjects.Task;

/**
 * Description of MyTasksAdapter.java
 *
 * @author Wesley Hager
 */
public class MyTasksAdapter extends ArrayAdapter<Task> {
    private List<Task> taskList;
    private Context mContext;
    private int mResource;

    public MyTasksAdapter(Context context, int resource, List<Task> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        taskList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);
        }

        final Task task = taskList.get(position);
        Project project = task.getProject();

        TextView title = (TextView) convertView.findViewById(R.id.title_cell_myTask);
        TextView description = (TextView) convertView.findViewById(R.id.description_txt_myTask);
        TextView deadline = (TextView) convertView.findViewById(R.id.deadline_cell_myTask);
        TextView projectName = (TextView) convertView.findViewById(R.id.projectName_myTask);
        final CheckBox checkbox = (CheckBox) convertView.findViewById(R.id.checkBox_myTask);

        title.setText(task.getTaskName());
        description.setText(task.getTaskDescription());

        if (!task.getDone()) {
            checkbox.setChecked(false);
            checkbox.setClickable(true);
            checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == AlertDialog.BUTTON_POSITIVE) {

                                task.setDone(true);
                                task.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            task.getProject().createNewMessage(task.getTaskName() + getContext().getString(R.string.msgComplete));
                                            task.setDone(true);
                                            notifyDataSetChanged();
                                        } else {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                            checkbox.setChecked(false);
                        }
                    };

                    AlertDialog.Builder al = new AlertDialog.Builder(getContext());
                    al.setTitle("Complete Task")
                            .setMessage("Are you sure? \nYou cannot UNCOMPLETE a Task!")
                            .setPositiveButton("Yes", listener)
                            .setNegativeButton("No", listener)
                            .show();
                }
            });
        } else {
            checkbox.setChecked(true);
            checkbox.setClickable(false);
        }
        Date date = task.getTaskDeadline();

        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(date.getTime());
            deadline.setText(DateFormat.format(Application.TIMESTAMP_FORMAT, date));


            if (cal.before(Calendar.getInstance()) && !task.getDone()) {
                deadline.setTextColor(mContext.getResources().getColor(R.color.deadlineLATE));
            } else {
                deadline.setTextColor(mContext.getResources().getColor(R.color.deadlineEARLY));
            }
        }

        if (project != null)
            projectName.setText(project.getProjectName());

        return convertView;
    }
}