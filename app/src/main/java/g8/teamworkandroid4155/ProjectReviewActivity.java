package g8.teamworkandroid4155;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseUser;

import g8.teamworkandroid4155.parseobjects.Project;

/**
 * Description of ProjectReviewActivity.java
 *
 * @author Wesley Hager
 */
public class ProjectReviewActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    private TextView tComm, tHelp, tPart, mUsername;
    private String idOther, idProject;
    private SeekBar sComm, sHelp, sPart;
    private ParseUser mOtherUser;
    private Button bSaveReview;
    private Project mProject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_review);

        idOther = getIntent().getStringExtra(Application.OTHER_USER);
        idProject = getIntent().getStringExtra(Application.CURRENT_PROJECT_ID);

        mUsername = (TextView) findViewById(R.id.username_ProjectReview);
        tComm = (TextView) findViewById(R.id.rank_project_communication_ProjectReview);
        tHelp = (TextView) findViewById(R.id.rank_project_helpfulness_ProjectReview);
        tPart = (TextView) findViewById(R.id.rank_project_participation_ProjectReview);
        bSaveReview = (Button) findViewById(R.id.saveReview_btn_ProjectReview);

        mProject = ParseObject.createWithoutData(Project.class, idProject);
        mOtherUser = ParseUser.createWithoutData(ParseUser.class, idOther);

        mOtherUser.fetchInBackground();
        mProject.fetchInBackground();

        sComm = (SeekBar) findViewById(R.id.seekBar_rank_project_communication_ProjectReview);
        sHelp = (SeekBar) findViewById(R.id.seekBar_rank_project_helpfulness_ProjectReview);
        sPart = (SeekBar) findViewById(R.id.seekBar_rank_project_participation_ProjectReview);

        sComm.setOnSeekBarChangeListener(this);
        sHelp.setOnSeekBarChangeListener(this);
        sPart.setOnSeekBarChangeListener(this);

        if (mOtherUser != null)
            mUsername.setText(mOtherUser.getUsername());

        bSaveReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProject.setHasPeerReviewed(mOtherUser, sComm.getProgress() + 1, sHelp.getProgress() + 1, sPart.getProgress() + 1);
                mProject.saveInBackground();
                finish();

            }
        });
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.seekBar_rank_project_communication_ProjectReview:
                tComm.setText(getResources().getTextArray(R.array.rank_communication)[progress]);
                break;
            case R.id.seekBar_rank_project_helpfulness_ProjectReview:
                tHelp.setText(getResources().getTextArray(R.array.rank_helpfulness)[progress]);
                break;
            case R.id.seekBar_rank_project_participation_ProjectReview:
                tPart.setText(getResources().getTextArray(R.array.rank_participation)[progress]);
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
