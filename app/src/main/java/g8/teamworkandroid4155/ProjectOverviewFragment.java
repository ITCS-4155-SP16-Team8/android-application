package g8.teamworkandroid4155;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import g8.teamworkandroid4155.parseobjects.Project;

/**
 * Description of ProjectOverviewFragment.java
 *
 * @author Wesley Hager
 */
public class ProjectOverviewFragment extends Fragment {
    private TextView pDescription, pName, pDeadline;
    private Project mProject;
    private Context mContext;

    public ProjectOverviewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getContext();
        mProject = ((MainActivity) mContext).getProject();
        View rootView = inflater.inflate(R.layout.fragment_project_details, container, false);

        pDescription = (TextView) rootView.findViewById(R.id.projectDescriptionContent_Members);
        pName = (TextView) rootView.findViewById(R.id.projectNameContent_Members);
        pDeadline = (TextView) rootView.findViewById(R.id.projectDeadlineContent_Members);

        pDescription.setText(mProject.getProjectDescription());
        pName.setText(mProject.getProjectName());
        pDeadline.setText(DateFormat.format(Application.TIMESTAMP_FORMAT, mProject.getProjectDeadline()));

        return rootView;
    }
}