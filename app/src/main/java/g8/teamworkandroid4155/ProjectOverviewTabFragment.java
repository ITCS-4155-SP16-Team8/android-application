package g8.teamworkandroid4155;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Description of ProjectOverviewTabFragment.java
 *
 * @author Wesley Hager
 */
public class ProjectOverviewTabFragment extends Fragment {

    private ProjectOverviewTabSectionAdapter mAdapter;
    private ViewPager viewPager;
    private OnFragmentInteractionListener mListener;
    private TabLayout tabLayout;
    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project_overview, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.projectOverviewContainer);
        tabLayout = (TabLayout) view.findViewById(R.id.projectToolBar);

        mContext = getContext();
        mAdapter = new ProjectOverviewTabSectionAdapter(getChildFragmentManager());
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(3);

        if (tabLayout != null)
            tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int pos);
    }

    public class ProjectOverviewTabSectionAdapter extends FragmentPagerAdapter {
        public ProjectOverviewTabSectionAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                return new ProjectOverviewFragment();
            }
            if (position == 1) {
                return new ProjectOverviewMemberFragment();
            }
            if (position == 2) {
                return new PieChartFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Details";
                case 1:
                    return "Members";
                case 2:
                    return "Chart";
            }
            return null;
        }
    }
}
