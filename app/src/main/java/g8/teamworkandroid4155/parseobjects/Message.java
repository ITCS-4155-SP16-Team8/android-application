package g8.teamworkandroid4155.parseobjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Description of Message.java
 *
 * @author Wesley Hager
 */
@ParseClassName("Message")
public class Message extends ParseObject {
    private static final String POSTED_BY = "postBy";

    /**
     * @return the content of this message
     */
    public String getMessageContent() {
        return getString("message");
    }

    public void setMessageContent(String s) {
        put("message", s);
    }

    /**
     * @return Username for this message, if null then "(Undefined)"
     */
    public String getMessagePostedBy() {
        return (getParseUser(POSTED_BY) != null) ? getParseUser(POSTED_BY).getUsername() : "(Undefined)";
    }

    /**
     * Sets this message posted by with a ParseUser
     *
     * @param user ParseUser that posts this message.
     */
    public void setMessagePostedBy(ParseUser user) {
        put(POSTED_BY, user);
    }

    /**
     * Sets a pointer to a project for this message
     *
     * @param project the project this message will be display in
     */
    public void setProject(Project project) {
        put("projectPointer", project);
    }
}
