package g8.teamworkandroid4155;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import g8.teamworkandroid4155.parseobjects.Project;

/**
 * Description of ProjectTabActivity.java
 *
 * @author Wesley Hager
 */
public class ProjectTabActivity extends Fragment {

    private Project mProject;
    private Bundle bundle;
    private ProjectTabSectionAdapter mAdapter;
    private ViewPager viewPager;
    private TaskExpandableListFragment taskExpandableListFragment;
    private MessageListFragment messageListFragment;
    private ProjectOverviewTabFragment projectOverviewTabFragment;
    private OnFragmentInteractionListener mListener;
    private TabLayout tabLayout;
    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_project_tab, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.projectContainer);
        tabLayout = (TabLayout) view.findViewById(R.id.projectTaskBar);

        mContext = getContext();
        mProject = ((MainActivity) mContext).getProject();
        mAdapter = new ProjectTabSectionAdapter(getChildFragmentManager());

        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 1)
                    messageListFragment.findMessages();
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1)
                    messageListFragment.findMessages();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void refreshTasks() {
        taskExpandableListFragment.updateTaskAdapter();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int pos);
    }

    public class ProjectTabSectionAdapter extends FragmentPagerAdapter {

        public ProjectTabSectionAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                projectOverviewTabFragment = new ProjectOverviewTabFragment();
                return projectOverviewTabFragment;
            }
            if (position == 1) {
                taskExpandableListFragment = new TaskExpandableListFragment();
                return taskExpandableListFragment;
            }
            if (position == 2) {
                messageListFragment = new MessageListFragment();
                return messageListFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Overview";
                case 1:
                    return "Tasks";
                case 2:
                    return "Messages";
            }
            return null;
        }
    }
}
