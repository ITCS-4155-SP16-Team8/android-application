package g8.teamworkandroid4155.parseobjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.Calendar;

@ParseClassName("Notification")
public class Notification extends ParseObject {

    public Task getTask() {
        return (Task) get("taskPointer");
    }

    /**
     * @return string with the task name and days remaining
     */
    public String getNotificationMsg() {
//        Task t = getTask();
//        return "\'" + t.getTaskName().trim() + "\' is due in " + (int) ((Calendar.getInstance().getTimeInMillis() - t.getTaskDeadline().getTime()) / (86400000)) + " days!";
        return getString("content") != null ? getString("content") : "Okay nothing here? why";
    }

    /**
     * @return if notification is unread then <code>false</code> else <code>true</code>
     */
    public boolean isRead() {
        return getBoolean("hasBeenRead");
    }

    /**
     * @return a Parse Query for notifications that are marked as unread
     */
    public static ParseQuery<Notification> getUnreadNotification() {
        return new ParseQuery<>(Notification.class).whereNotEqualTo("hasBeenRead", true).include("taskPointer");
    }

    /**
     * @param b <code>true</code> has been read, <code>false</code> has not been read
     */
    public void setHasBeenRead(boolean b) {
        put("hasBeenRead", b);
    }
}
