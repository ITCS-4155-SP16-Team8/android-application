package g8.teamworkandroid4155.parseobjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

/**
 * Description of Task.java
 *
 * @author Wesley Hager
 */
@ParseClassName("Task")
public class Task extends ParseObject implements Serializable {

    // Get Methods
    public String getTaskName() {
        return getString("taskName");
    }

    // Set Methods
    public void setTaskName(String tName) {
        put("taskName", tName);
    }

    public String getTaskDescription() {
        return getString("taskDescription");
    }

    public void setTaskDescription(String tDescription) {
        put("taskDescription", tDescription);
    }

    public Date getTaskDeadline() {
        return getDate("taskDeadline");
    }

    public void setTaskDeadline(Date tDeadline) {
        put("taskDeadline", tDeadline);
    }

    public ParseUser getTaskUser() {
        return getParseUser("createdBy");
    }

    public void setTaskUser(ParseUser tUser) {
        put("createdBy", tUser);
    }

    public String getAssignedTo() {
        return (get("assignedTo") != null) ? ((ParseUser) get("assignedTo")).getUsername() : "(Unassigned)";
    }

    public void setAssignedTo(ParseUser user) {
        put("assignedTo", user);
    }

    public String getAssignedToID() {
        return (get("assignedTo") != null) ? ((ParseUser) get("assignedTo")).getObjectId() : "";
    }

    public ParseUser getCreatedBy() {
        return getParseUser("createdBy");
    }

    public boolean getDone() {
        return getBoolean("done");
    }

    public void setDone(Boolean b) {
        put("done", b);
    }

    public void setProject(Project project) {
        put("projectPointer", project);
    }

    public void setAssignedToNothing() {
        remove("assignedTo");
    }

    public double getFinalPoints() {
        return getDouble("finalPoints");
    }

    public Project getProject(){
        return (Project) get("projectPointer");
    }

    private Object createRating(String userID, int p) {
        JSONObject j = new JSONObject();

        try {
            j.put("rater", userID);
            j.put("rating", p);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return j;
    }

    public void setHasRated(ParseUser mUser, int point) {
        JSONArray j = getJSONArray("totalRatings");
        if (j == null)
            j = new JSONArray();

        j.put(createRating(mUser.getObjectId(), point));

        put("totalRatings", j);
    }

    public boolean getHasRated(ParseUser u) {
        JSONArray j = getJSONArray("totalRatings");
        if (j != null) {
            for (int i = 0; i < j.length(); i++) {
                try {
                    if (j.getJSONObject(i).getString("rater").equals(u.getObjectId()))
                        return true;
                } catch (JSONException ignored) {
                }
            }
        }
        return false;
    }

    public String getTaskRating(){
        JSONArray j = getJSONArray("totalRatings");
        double total = 0;
        if (j != null) {
            for (int i = 0; i < j.length(); i++) {
                try {
                    total += j.getJSONObject(i).getInt("rating");
                } catch (JSONException ignored) {
                }
            }
            return String.format("%2.2f (%d)", total/j.length(), j.length());
        }
        return "N/A";

    }
}
