package g8.teamworkandroid4155.initializeuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import g8.teamworkandroid4155.DispatchActivity;
import g8.teamworkandroid4155.R;

/**
 * Description of SignupActivity.java
 *
 * @author Wesley Hager
 */
public class SignupActivity extends AppCompatActivity {

    private EditText userName, email, password, passwordAgain;
    private Button submit;
    private String mUsername, mEmail, mPassword, mPasswordAgain;
    private ProgressDialog signupProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        submit = (Button) findViewById(R.id.signup_Signup);
        userName = (EditText) findViewById(R.id.username_Signup);
        email = (EditText) findViewById(R.id.email_Signup);
        password = (EditText) findViewById(R.id.password_Signup);
        passwordAgain = (EditText) findViewById(R.id.passwordagain_Signup);

        signupProgress = new ProgressDialog(this);
        signupProgress.setTitle(getString(R.string.signingUpProgressDialog));
        signupProgress.setCancelable(false);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUserInput();
            }
        });
    }

    private void validateUserInput() {
        mEmail = email.getText().toString();
        mPassword = password.getText().toString();
        mPasswordAgain = passwordAgain.getText().toString();
        mUsername = userName.getText().toString();

        // TODO: VALIDATE USERNAME, EMAIL, PASSWORD
        if (!mPassword.contains(" ")) {
            if (mPassword.equals(mPasswordAgain)) {
                if (mEmail.contains("@"))
                    attemptSignup();
            } else {
                Toast.makeText(SignupActivity.this, "Passwords Must Match", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(SignupActivity.this, "Password Cannot Contain Spaces", Toast.LENGTH_SHORT).show();
        }
    }

    private void attemptSignup() {
        signupProgress.show();

        ParseUser user = (ParseUser) ParseObject.create("_User");

        user.setEmail(mEmail);
        user.setUsername(mUsername);
        user.setPassword(mPassword);

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    startActivity(new Intent(SignupActivity.this, DispatchActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                } else {
                    switch (e.getCode()) {
                        case ParseException.EMAIL_TAKEN:
                            Toast.makeText(SignupActivity.this, "Email Taken", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.USERNAME_TAKEN:
                            Toast.makeText(SignupActivity.this, "Username Taken", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.ACCOUNT_ALREADY_LINKED:
                            Toast.makeText(SignupActivity.this, "Account Already Linked", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            e.printStackTrace();
                    }
                }

                signupProgress.dismiss();
            }
        });
    }
}
