package g8.teamworkandroid4155;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.parse.ParseUser;

import g8.teamworkandroid4155.initializeuser.LoginActivity;

/**
 * Description of DispatchActivity.java
 * launches an activity if ParseUser is currently logged in
 *
 * @author Wesley Hager
 */
public class DispatchActivity extends Activity {

    public DispatchActivity() {
        // Requires Empty Constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ParseUser.getCurrentUser() != null) {
            startActivity(new Intent(DispatchActivity.this, MainActivity.class));
        } else {
            startActivity(new Intent(DispatchActivity.this, LoginActivity.class));
        }
    }

}
