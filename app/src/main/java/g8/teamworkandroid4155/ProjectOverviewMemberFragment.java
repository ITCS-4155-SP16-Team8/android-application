package g8.teamworkandroid4155;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseRole;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import g8.teamworkandroid4155.parseobjects.Project;

public class ProjectOverviewMemberFragment extends Fragment {
    private OnFragmentInteractionListener mListener;

    private EditText text;
    private ListView usersListView;
    private Button reloadMembers;
    private Project mProject;
    private Context mContext;
    private InputMethodManager imm;

    private ProjectOverviewMemberAdapter parseUserArrayAdapter;

    private List<ParseUser> usersList = new ArrayList<>();
    private boolean current = false;
    private ParseRole mRoleAddUser;

    public ProjectOverviewMemberFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project_overview_member, container, false);
        imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        mContext = getContext();
        mProject = ((MainActivity) mContext).getProject();
        text = (EditText) view.findViewById(R.id.searchUser_text_Members);
        reloadMembers = (Button) view.findViewById(R.id.reloadMembers_Members);


        usersListView = (ListView) view.findViewById(R.id.users_list_AddUsers_Members);

        usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!current) {
                    addUser(usersList.get(position));
                }
            }
        });

        parseUserArrayAdapter = new ProjectOverviewMemberAdapter(getContext(), R.layout.cell_user_details, usersList, mProject);
        parseUserArrayAdapter.setNotifyOnChange(true);
        usersListView.setAdapter(parseUserArrayAdapter);

        view.findViewById(R.id.searchNewUser_button_Members).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search = text.getText().toString();
                if (!search.equals(""))
                    searchUsers(text.getText().toString().trim());
            }
        });

        reloadMembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text.setText("");
                loadCurrentMembers();
            }
        });

        mRoleAddUser = (ParseRole) mProject.get("projectRole");
        loadCurrentMembers();

        return view;
    }

    private void loadCurrentMembers() {
        ParseRelation<ParseUser> mUsers = mRoleAddUser.getUsers();
        mUsers.getQuery().findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
                if (e == null) {
                    current = true;
                    parseUserArrayAdapter.setAllowRatings(true);
                    usersList.clear();
                    usersList.addAll(list);

                    loadUserList();
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    private void searchUsers(String search) {
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

        ParseQuery<ParseUser> q = ParseQuery.getQuery("_User");
        q.whereContains("username", search);
        q.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
                if (e == null) {
                    current = false;
                    parseUserArrayAdapter.setAllowRatings(false);
                    usersList.clear();
                    usersList.addAll(list);
                    loadUserList();
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    private void loadUserList() {
        if (usersList.size() > 0) {
            reloadMembers.setText("(Tap to Refresh)");
        } else {
            reloadMembers.setText(R.string.reloadCurrentMembers);
        }
        parseUserArrayAdapter.notifyDataSetChanged();
    }

    private void addUser(final ParseUser u) {
        mRoleAddUser.getUsers().add(u);
        ParseACL acl = mRoleAddUser.getACL();
        acl.setReadAccess(u, true);
        acl.setWriteAccess(u, true);
        mRoleAddUser.setACL(acl);
        mRoleAddUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(getContext(), "Added User", Toast.LENGTH_SHORT).show();
                    mProject.createNewMessage(u.getUsername() + getContext().getString(R.string.msgAddUser));
                    loadCurrentMembers();
                } else {
                    Toast.makeText(getContext(), "Denied", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int pos);
    }
}
