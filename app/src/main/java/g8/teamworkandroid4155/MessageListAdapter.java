package g8.teamworkandroid4155;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import g8.teamworkandroid4155.parseobjects.Message;

/**
 * Description of MessageListAdapter.java
 * manages display of message objects in a ListView
 *
 * @author Wesley Hager
 */
public class MessageListAdapter extends ArrayAdapter<Message> {
    private List<Message> messageList;
    private Context mContext;
    private int mResource;

    public MessageListAdapter(Context context, int resource, List<Message> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        this.messageList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);
        }

        Message message = messageList.get(position);

        TextView messageContent = (TextView) convertView.findViewById(R.id.content_Message);
        TextView postedBy = (TextView) convertView.findViewById(R.id.postedBy_Message);
        TextView timeStamp = (TextView) convertView.findViewById(R.id.timeStamp_Message);

        messageContent.setText(message.getMessageContent());
        postedBy.setText(message.getMessagePostedBy());
        timeStamp.setText(DateFormat.format(Application.TIMESTAMP_FORMAT_WITHHOUR, (message.getObjectId() != null) ? message.getCreatedAt() : new Date()));

        return convertView;
    }
}
