package g8.teamworkandroid4155.initializeuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import g8.teamworkandroid4155.DispatchActivity;
import g8.teamworkandroid4155.R;

/**
 * Description of LoginActivity.java
 *
 * @author Wesley Hager
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LogInCallback {

    private EditText ePassword, eEmail;
    private Button loginButton, signupButton, forgotPasswordButton;
    private ProgressDialog loginProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ePassword = (EditText) findViewById(R.id.password_Login);
        eEmail = (EditText) findViewById(R.id.email_Login);
        loginButton = (Button) findViewById(R.id.login_button_Login);
        signupButton = (Button) findViewById(R.id.signup_button_Login);
        forgotPasswordButton = (Button) findViewById(R.id.forgotPassword_button_Login);

        loginButton.setOnClickListener(this);
        signupButton.setOnClickListener(this);
        forgotPasswordButton.setOnClickListener(this);

        loginProgress = new ProgressDialog(this);
        loginProgress.setTitle(getString(R.string.loginProgressDialog));
        loginProgress.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login_button_Login) {
            attemptLogin(eEmail.getText().toString(), ePassword.getText().toString());
        } else if (v.getId() == R.id.signup_button_Login) {
            startActivity(new Intent(LoginActivity.this, SignupActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }


//        else if (v.getId() == R.id.forgotPassword_button_Login) {
//            // TODO: Add forgot password
//        }
    }

    private void attemptLogin(String email, String password) {
        loginProgress.show();
        ParseUser.logInInBackground(email, password, this);
    }

    @Override
    public void done(ParseUser parseUser, ParseException e) {
        loginProgress.dismiss();

        // Successful login
        if (e == null && parseUser != null) {
            loginSuccessful();

            // Failed to login
        } else if (e != null) {
            Toast.makeText(LoginActivity.this, "Email/Password Wrong", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void loginSuccessful() {
        startActivity(new Intent(LoginActivity.this, DispatchActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY));
    }
}
