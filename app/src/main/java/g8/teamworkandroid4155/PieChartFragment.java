package g8.teamworkandroid4155;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import g8.teamworkandroid4155.parseobjects.Project;
import g8.teamworkandroid4155.parseobjects.Task;

public class PieChartFragment extends Fragment {
    private Context mContext;
    private Project mProject;
    private PieChart chart;
    private OnFragmentInteractionListener mListener;
    private HashMap<String, Float> pieChartData;

    public PieChartFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getContext();
        mProject = ((MainActivity) mContext).getProject();

        View view = inflater.inflate(R.layout.fragment_pie_chart, container, false);
        Button refresh = (Button) view.findViewById(R.id.refresh_Chart);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPieChart();
            }
        });
        chart = (PieChart) view.findViewById(R.id.chart);
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        loadPieChart();

        return view;
    }

    public void loadPieChart() {
        pieChartData = new HashMap<>();

        ParseQuery<Task> tQuery;
        tQuery = ParseQuery.getQuery("Task");
        tQuery.include("assignedTo");
        tQuery.include("projectPointer");
        tQuery.include("createdBy");
        tQuery.whereEqualTo("projectPointer", mProject);
        tQuery.orderByAscending("taskDeadline");

        tQuery.findInBackground(new FindCallback<Task>() {
            @Override
            public void done(List<Task> list, ParseException e) {
                if (e == null) {

                    int total = 0;
                    for (Task t : list)
                        if (!pieChartData.containsKey(t.getAssignedTo())) {
                            pieChartData.put(t.getAssignedTo(), 1f);
                            total++;
                        } else {
                            pieChartData.put(t.getAssignedTo(), pieChartData.get(t.getAssignedTo()) + 1);
                            total++;
                        }

                    List<Entry> entries = new ArrayList<>();
                    List<String> names = new ArrayList<>();
                    int i = 0;
                    for (String key : pieChartData.keySet()) {
                        names.add(key);
                        entries.add(new Entry(pieChartData.get(key), i++));
                    }
                    chart.setCenterText("Task Count is " + total);
                    chart.setRotationEnabled(false);
                    chart.setDescription("Distribution of Tasks");

                    PieDataSet dataSet = new PieDataSet(entries, "");
                    dataSet.addColor(Color.YELLOW);
                    dataSet.addColor(Color.RED);
                    dataSet.addColor(Color.BLUE);
                    dataSet.setSliceSpace(3);

                    PieData data = new PieData(names, dataSet);
                    data.setValueTextSize(14);

                    chart.setData(data);
                    chart.invalidate();
                } else {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
