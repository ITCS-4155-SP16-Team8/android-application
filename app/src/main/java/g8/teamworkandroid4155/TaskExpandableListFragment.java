package g8.teamworkandroid4155;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Spinner;

import com.baoyz.widget.PullRefreshLayout;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseRole;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import g8.teamworkandroid4155.parseobjects.Project;
import g8.teamworkandroid4155.parseobjects.Task;

/**
 * Description of TaskExpandableListFragment.java
 *
 * @author Wesley Hager
 */
public class TaskExpandableListFragment extends Fragment implements AdapterView.OnItemSelectedListener, AdapterView.OnItemLongClickListener {

    private Context mContext;
    private ExpandableListView tasksListView;
    private List<Task> tasks;
    private TaskExpandableListAdapter adapter = null;
    private Spinner sortSpinner;
    private Button createTask;
    private Project mProject;
    private ParseRole mRoleTaskList;
    private ArrayList<ParseUser> usersListTask;
    private PullRefreshLayout pullRefreshLayout;
    private OnFragmentInteractionListener mListener;
    private int currentFilter = 0;
    private ParseQuery<Task> tQuery;

    public TaskExpandableListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getContext();
        mProject = ((MainActivity) mContext).getProject();

        usersListTask = new ArrayList<>();

        View view = inflater.inflate(R.layout.fragment_task_list, container, false);
        mRoleTaskList = (ParseRole) mProject.get("projectRole");
        createTask = (Button) view.findViewById(R.id.createTask_button_Task);
        tasksListView = (ExpandableListView) view.findViewById(R.id.tasks_listView_Task);
        sortSpinner = (Spinner) view.findViewById(R.id.tasksSort_Spinner_Task);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        createTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(MainActivity.LOAD_CREATE_TASK);
            }
        });

        tasks = new ArrayList<>();

        adapter = new TaskExpandableListAdapter(mContext, tasks, getChildFragmentManager(), mProject, TaskExpandableListFragment.this);
        tasksListView.setAdapter(adapter);
        sortSpinner.setOnItemSelectedListener(this);
        tasksListView.setOnItemLongClickListener(this);

        loadCurrentMembers();
        findTasks();

        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                findTasks();
            }
        });

        return view;
    }

    private void loadCurrentMembers() {
        ParseRelation<ParseUser> mUsers = mRoleTaskList.getUsers();
        mUsers.getQuery().findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
                if (e == null) {
                    usersListTask.clear();
                    usersListTask.addAll(list);
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    public void updateTaskAdapter(){
        adapter.notifyDataSetChanged();
    }

    public void findTasks() {
        pullRefreshLayout.setRefreshing(true);
        if (tQuery == null) {
            tQuery = ParseQuery.getQuery("Task");
            tQuery.include("assignedTo");
            tQuery.include("projectPointer");
            tQuery.include("createdBy");
            tQuery.whereEqualTo("projectPointer", mProject);
            tQuery.orderByAscending("done");
            tQuery.addAscendingOrder("taskDeadline");
        }
        tQuery.findInBackground(new FindCallback<Task>() {
            @Override
            public void done(List<Task> list, ParseException e) {
                pullRefreshLayout.setRefreshing(false);
                if (e == null) {
                    loadList(list);
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    public void taskWasUpdated(Task t, int pos) {
        tasks.get(pos).setDone(true);
        adapter.notifyDataSetInvalidated();
    }

    private void loadList(List<Task> task) {
        tasks.clear();
        tasks.addAll(task);
        adapter.replaceOriginalList(task);
        sortSpinner.setSelection(currentFilter, false);
        adapter.getFilter().filter(getResources().getStringArray(R.array.taskSort)[currentFilter]);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Application.CREATE_NEW_TASK:
                findTasks();
                break;
            case Application.TASK_DELETED:
                findTasks();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // Todo: filter to sort adapterValues
        if (adapter != null) {
            currentFilter = position;
            adapter.getFilter().filter(getResources().getStringArray(R.array.taskSort)[position]);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        final Task task = tasks.get(position);
        if (!task.getDone()) {
            // If Assigned User is current user then allow to them to mark it complete
            if (task.getAssignedToID().equals(ParseUser.getCurrentUser().getObjectId())) {
                AlertDialog.Builder al = new AlertDialog.Builder(mContext);
                al.setTitle("Complete Task")
                        .setMessage("Are you sure? \nYou cannot UNCOMPLETE a Task!")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                task.setDone(true);
                                task.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            mProject.createNewMessage(task.getTaskName() + mContext.getString(R.string.msgComplete));
                                            adapter.notifyDataSetChanged();
                                        } else {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        }
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public ArrayList<ParseUser> getUsers() {
        return usersListTask;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int pos);
    }
}
