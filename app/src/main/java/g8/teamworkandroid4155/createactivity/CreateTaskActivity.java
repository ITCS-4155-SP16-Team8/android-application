package g8.teamworkandroid4155.createactivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Calendar;

import g8.teamworkandroid4155.Application;
import g8.teamworkandroid4155.MainActivity;
import g8.teamworkandroid4155.R;
import g8.teamworkandroid4155.parseobjects.Project;
import g8.teamworkandroid4155.parseobjects.Task;

/**
 * Description of CreateTaskActivity.java
 *
 * @author Wesley Hager
 */
public class CreateTaskActivity extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final String TAG = "CreateTask";
    private EditText tName, tDescription, tDate;
    private Calendar date = Calendar.getInstance();
    private Task task;
    private Button createTaskButton;
    private Button deleteTaskButton;
    private DatePickerDialog datePicker;
    private Project mProject;
    private Task mTask;
    private Context mContext;

    private OnFragmentInteractionListener mListener;

    public CreateTaskActivity() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getContext();
        mProject = ((MainActivity) mContext).getProject();
        mTask = ((MainActivity) mContext).getTask();

        View view = inflater.inflate(R.layout.activity_create_task, container, false);

        // MARK: - Assign GUI variables
        createTaskButton = (Button) view.findViewById(R.id.saveTask_button_CreateTask);
        deleteTaskButton = (Button) view.findViewById(R.id.deleteTask_Button_CreateTask);
        tDate = (EditText) view.findViewById(R.id.taskDate_CreateTask);
        tName = (EditText) view.findViewById(R.id.taskName_CreateTask);
        tDescription = (EditText) view.findViewById(R.id.taskDescription_CreateTask);

        createTaskButton.setOnClickListener(this);
        deleteTaskButton.setOnClickListener(this);
        tDate.setOnClickListener(this);

        if (mTask != null) {


            date.setTime(mTask.getTaskDeadline());
            deleteTaskButton.setVisibility(View.VISIBLE);
            tName.setText(mTask.getTaskName());
            tDescription.setText(mTask.getTaskDescription());
            tDate.setText(DateFormat.format(Application.TIMESTAMP_FORMAT, date));
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveTask_button_CreateTask:
                validateTaskInput();
                break;
            case R.id.taskDate_CreateTask:
                loadDatePicker();
                break;
            case R.id.deleteTask_Button_CreateTask:
                deleteTask();
                break;
        }
    }

    private void deleteTask() {
        AlertDialog.Builder al = new AlertDialog.Builder(mContext);
        al.setTitle("Deleting Task")
                .setMessage(R.string.areYouSure)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mTask.deleteInBackground(new DeleteCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    mProject.createNewMessage(mTask.getTaskName() + getString(R.string.msgDelete));
                                    mListener.onFragmentInteraction(MainActivity.LOAD_CURRENT_PROJECT);
                                }
                            }
                        });
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private void validateTaskInput() {
        Boolean bool = false;
        // Todo: Validate TaskInputs
        if (mProject != null) {
            if (!tDate.getText().toString().equals("") && !tName.getText().toString().equals("")) {
                createTask();
            } else {
                Toast.makeText(mContext, "Task Name & Deadline (Required)", Toast.LENGTH_SHORT).show();
                tDate.setHintTextColor(Color.RED);
                tName.setHintTextColor(Color.RED);
            }
        } else {
            // Todo: Remove
            Toast.makeText(mContext, "Try Again didn't get project yet.", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateSaveButton(boolean saving) {
        if (saving) {
            createTaskButton.setText(R.string.savingTask_btn);
            createTaskButton.setTextColor(Color.GREEN);
            createTaskButton.setEnabled(false);
        } else {
            createTaskButton.setText(R.string.save_task_Task);
            createTaskButton.setTextColor(Color.BLACK);
            createTaskButton.setEnabled(true);
        }
    }

    private void createTask() {
        updateSaveButton(true);
        boolean postedMsg = false;

        if (mTask == null) {
            task = new Task();
            task.setACL(mProject.getACL());
            task.setDone(false);
            task.setProject(mProject);
            task.setTaskUser(ParseUser.getCurrentUser());
        } else {
            task = mTask;
            if (!task.getTaskName().equals(tName.getText().toString())) {
                postedMsg = true;
                mProject.createNewMessage("Task \'" + task.getTaskName() + "\' was renamed to \'" + tName.getText().toString() + "\'.");
            }
        }

        if (!postedMsg)
            mProject.createNewMessage(task.getTaskName() + getString(R.string.msgEdit));


        task.setTaskName(tName.getText().toString());
        task.setTaskDescription(tDescription.getText().toString());
        task.setTaskDeadline(date.getTime());

        task.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    if (mTask != null) {
                        Toast.makeText(mContext, "Updated Task", Toast.LENGTH_SHORT).show();
                        mListener.onFragmentInteraction(MainActivity.LOAD_CURRENT_PROJECT);
                    } else {
                        mProject.createNewMessage(task.getTaskName() + getString(R.string.msgCreate));
                        addRelation();
                    }
                } else {
                    e.printStackTrace();
                    updateSaveButton(false);
                    Toast.makeText(mContext, "Error Saving Task", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addRelation() {
        ParseRelation<Task> relation = mProject.getProjectRelationToTask();
        relation.add(task);

        task.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(mContext, "Created Task", Toast.LENGTH_SHORT).show();
                    mListener.onFragmentInteraction(MainActivity.LOAD_CURRENT_PROJECT);
                } else {
                    updateSaveButton(false);
                    Toast.makeText(mContext, "Failed to make relation", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadDatePicker() {
        datePicker = new DatePickerDialog(mContext, this, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH) + 1);
        datePicker.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        tDate.setText(String.format("%d/%02d/%d", monthOfYear + 1, dayOfMonth, year));
        date.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int pos);
    }
}
