package g8.teamworkandroid4155;

import com.parse.Parse;
import com.parse.ParseObject;

import g8.teamworkandroid4155.parseobjects.Message;
import g8.teamworkandroid4155.parseobjects.Notification;
import g8.teamworkandroid4155.parseobjects.ParUser;
import g8.teamworkandroid4155.parseobjects.Project;
import g8.teamworkandroid4155.parseobjects.Task;

/**
 * Description of Application.java
 * initializes Parse with applicationId and clientId
 *
 * @author Wesley Hager
 */
public class Application extends android.app.Application {

    public static final String CURRENT_PROJECT_ID = "PROJECT_ID";
    public static final String CURRENT_PROJECT_ROLE = "PROJECT_ROLE";
    public static final String CURRENT_PROJECT_NAME = "PROJECT_NAME";
    public static final String CURRENT_PROJECT_DESCRIPTION = "PROJECT_DESC";
    public static final String CURRENT_PROJECT_RELATION_TASK = "PROJECT_TASK";
    public static final String CURRENT_PROJECT_RELATION_MESSAGE = "PROJECT_MESSAGE";
    public static final String CURRENT_PROJECT_USERS = "PROJECT_USERS";

    public static final String CURRENT_PROJECT_USER_TO_RATE = "CURRENT_PROJECT_USER_TO_RATE";

    public static final String TASK_VALUES = "TASK_VALUES";
    public static final String TASK_NAME = "TASK_NAME";
    public static final String TASK_DESCRIPTION = "TASK_DESCRIPTION";
    public static final String TASK_DATE = "TASK_DATE";
    public static final String TASK_ASSIGNED = "TASK_ASSIGNED";

    public static final int SAVED_NEW_PROJECT = 0x2;
    public static final int CREATE_NEW_TASK = 0x4;
    public static final int UPDATE_TASK = 0x6;
    public static final int TASK_DELETED = 0x8;

    public static final String TIMESTAMP_FORMAT = "MM/dd/yyy";
    public static final String TIMESTAMP_FORMAT_WITHHOUR = "MM/dd/yyyy  hh:mm a";
    public static final String OTHER_USER = "otherUser";

    @Override
    public void onCreate() {
        super.onCreate();

        ParseObject.registerSubclass(Project.class);
        ParseObject.registerSubclass(Task.class);
        ParseObject.registerSubclass(Message.class);
        ParseObject.registerSubclass(ParUser.class);
        ParseObject.registerSubclass(Notification.class);
        Parse.enableLocalDatastore(getApplicationContext());
        Parse.initialize(this, "bWAeh72kpaBUWySXJDqDZBvGZE0YARSNiEDuxXYx",
                "SpCO7PuC0DuZDiD0OFGmhy3ON0ujQYj88KXzFEqY");
    }
}
