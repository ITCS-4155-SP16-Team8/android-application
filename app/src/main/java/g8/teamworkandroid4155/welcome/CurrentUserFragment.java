package g8.teamworkandroid4155.welcome;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import g8.teamworkandroid4155.R;
import g8.teamworkandroid4155.parseobjects.ParUser;

public class CurrentUserFragment extends Fragment {

    public static final int IMG_RESULT = 0x30;

    private TextView displayName;
    private TextView email;
    private RatingBar sComm;
    private RatingBar sHelp;
    private RatingBar sPart;
    private RatingBar sTotal;
    private ParUser user;

    //    private ProgressDialog progressDialog;
    private ImageView userImage;

    public CurrentUserFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_user, container, false);

        displayName = (TextView) view.findViewById(R.id.displayName_currentUser);
        email = (TextView) view.findViewById(R.id.email_currentUser);
        sComm = (RatingBar) view.findViewById(R.id.comStat_currentUser);
        sHelp = (RatingBar) view.findViewById(R.id.helpStat_currentUser);
        sPart = (RatingBar) view.findViewById(R.id.partStat_currentUser);
        sTotal = (RatingBar) view.findViewById(R.id.totalRating_rBar_currentUser);
        userImage = (ImageView) view.findViewById(R.id.userimage_currentUser);

        user = (ParUser) ParseUser.getCurrentUser();

        user.fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    user = (ParUser) parseObject;
                    setupUserDetails();
                } else {
                    e.printStackTrace();
                }
            }
        });

        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getContext(), "Missing Gallery Permissions", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, IMG_RESULT);
                }
            }
        });

        return view;
    }

    private void setupUserDetails() {
        displayName.setText(user.getUsername());
        email.setText(user.getEmail());
        sComm.setRating((float) user.getCommunicationStat());
        sHelp.setRating((float) user.getHelpfulnessStat());
        sPart.setRating((float) user.getParticipationStat());
        sTotal.setRating((float) user.getTotalStat());

        ParseFile file = user.getParseFile("picture");
        if (file != null)
            displayUserPicture(file);

    }

    private void displayUserPicture(ParseFile file) {
        file.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] bytes, ParseException e) {
                if (e == null) {
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    userImage.setImageBitmap(bmp);
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    public void updateProfileImage(Bitmap img){
        userImage.setImageBitmap(img);
    }
}
