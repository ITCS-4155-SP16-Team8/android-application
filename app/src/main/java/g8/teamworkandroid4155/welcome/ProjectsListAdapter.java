package g8.teamworkandroid4155.welcome;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import g8.teamworkandroid4155.R;
import g8.teamworkandroid4155.parseobjects.Project;

public class ProjectsListAdapter extends ArrayAdapter<Project> {
    private List<Project> projectArrayList;
    private Context mContext;
    private int mResource;

    public ProjectsListAdapter(Context context, int resource, List<Project> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        this.projectArrayList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell_projects, parent, false);
        }

        Project p = projectArrayList.get(position);

        TextView projectName = (TextView) convertView.findViewById(R.id.projectName_cellProject);
        projectName.setText(p.getProjectName());

        RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.projectContainer_cell_Project);

        if (p.getProjectDone())
            rl.setBackgroundColor(Color.GRAY);
        else
            rl.setBackgroundColor(0);

        return convertView;
    }
}
