package g8.teamworkandroid4155;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import g8.teamworkandroid4155.parseobjects.Notification;

/**
 * Description of NotificationDialogFragment.java
 * Displays fragment with a list of notifications and the option
 * to set a notification as read.
 *
 * @author Wesley Hager
 */
public class NotificationDialogFragment extends DialogFragment {
    private List<Notification> notifications = new ArrayList<>();
    private ListView listView;
    private ArrayAdapter adapter;

    public void setNotification(List<Notification> notification) {
        notifications = notification;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.fragment_notification_dialog, null);

        listView = (ListView) v.findViewById(R.id.notificationList_Notify);
        adapter = new NotificationDialogListAdapter(getContext(), R.layout.cell_notification, notifications);
        listView.setAdapter(adapter);

        builder.setView(v)
                .setMessage("Notifications:\nCheck to remove")
                .setCancelable(true);
        return builder.create();
    }
}
