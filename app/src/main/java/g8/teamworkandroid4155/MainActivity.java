package g8.teamworkandroid4155;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import g8.teamworkandroid4155.createactivity.CreateProjectActivity;
import g8.teamworkandroid4155.createactivity.CreateTaskActivity;
import g8.teamworkandroid4155.parseobjects.Notification;
import g8.teamworkandroid4155.parseobjects.ParUser;
import g8.teamworkandroid4155.parseobjects.Project;
import g8.teamworkandroid4155.parseobjects.Task;
import g8.teamworkandroid4155.welcome.ProjectsFragment;
import g8.teamworkandroid4155.welcome.UserMainActivity;

/**
 * Description of MainActivity.java
 * Handles navigation between all fragments, maintains list of projects for fragments to reference.
 *
 * @author Wesley Hager
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        ProjectsFragment.OnFragmentInteractionListener, UserMainActivity.OnFragmentInteractionListener,
        CreateProjectActivity.OnFragmentInteractionListener, ProjectTabActivity.OnFragmentInteractionListener,
        TaskExpandableListFragment.OnFragmentInteractionListener, CreateTaskActivity.OnFragmentInteractionListener,
        TaskRatingDialogFragment.taskRatedListener, MyTasksFragment.OnFragmentInteractionListener, TaskExpandableListAdapter.onFragmentEditTask,
        PieChartFragment.OnFragmentInteractionListener, ProjectOverviewTabFragment.OnFragmentInteractionListener,
        ProjectOverviewMemberFragment.OnFragmentInteractionListener {

    public static final int CONTAINER = R.id.projectContainer;
    public static final int LOAD_USERMAIN = 0x20;
    public static final int LOAD_CREATE_PROJECT = 0x21;
    public static final int LOAD_CREATE_TASK = 0x22;
    public static final int LOAD_CURRENT_PROJECT = 0x23;
    public static final String CREATE_PROJECT = "createProject";
    public static final String RATEPROJECT = "rateProject";
    private static final String TAG = "MAINACTIVITY";

    private Menu projectMenu;
    private List<Project> mProjectList = new ArrayList<>();
    private List<Notification> mNotification = new ArrayList<>();
    private TextView navUsername, navEmail, navNotification;
    private ImageView navImage;
    private FragmentManager fragmentManager;
    private ProjectTabActivity projectTabActivity;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private int selectedProjectPos;
    private boolean isCreatingTask = false;
    private ProgressBar navProgress;
    private Toolbar toolbar;
    private Task ratingTask;
    private UserMainActivity userMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(MainActivity.this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                findProjects();
                findNotifications();
            }
        });

        if (navigationView != null) {
            View view = navigationView.getHeaderView(0);
            navUsername = (TextView) view.findViewById(R.id.username_navBar);
            navEmail = (TextView) view.findViewById(R.id.email_navBar);
            navImage = (ImageView) view.findViewById(R.id.profile_img_navBar);
            navProgress = (ProgressBar) view.findViewById(R.id.progress_navBar);
            navNotification = (TextView) view.findViewById(R.id.notification_txt_navBar);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadUserMain();
                }
            });

            navNotification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (Notification n : mNotification)
                        Log.d(TAG, "onClick: " + n.getNotificationMsg());

                    NotificationDialogFragment dialogFragment = new NotificationDialogFragment();
                    dialogFragment.setNotification(mNotification);
                    dialogFragment.show(fragmentManager, "Notification");
                }
            });

            findNotifications();

            navUsername.setText(ParseUser.getCurrentUser().getUsername());
            navEmail.setText(ParseUser.getCurrentUser().getEmail());
            navImage.setImageResource(R.drawable.personimage);

            ParseFile file = ParseUser.getCurrentUser().getParseFile("picture");
            if (file != null) {
                file.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] bytes, ParseException e) {
                        if (e == null) {
                            Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            navImage.setImageBitmap(bmp);
                        } else {
                            e.printStackTrace();
                        }
                    }
                });
            }


            projectMenu = navigationView.getMenu();
            navigationView.setNavigationItemSelectedListener(MainActivity.this);

            projectMenu.add(0, 0, 0, "Logout");
            projectMenu.add(0, 1, 0, R.string.create_a_project);
        }

        fragmentManager = getSupportFragmentManager();

        loadUserMain();
        findProjects();
    }

    private void findNotifications() {
        ParseQuery<Notification> nParseQuery = Notification.getUnreadNotification();
        nParseQuery.findInBackground(new FindCallback<Notification>() {
            @Override
            public void done(List<Notification> list, ParseException e) {
                if (e == null) {
                    navNotification.setText(list.size() + "");
                    mNotification.clear();
                    mNotification.addAll(list);
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Close navigation Drawers
        // if menu group equals 1 load project
        // else it was a default menu option
        drawer.closeDrawers();
        if (item.getGroupId() == 1) {
            loadProjectTab(item.getItemId());
        } else {
            switch (item.getItemId()) {
                case 0:
                    logout();
                    break;
                case 1:
                    loadCreateProject();
                    break;
            }
        }

        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onFragmentInteraction(int pos) {
        // Loads Fragments based on pos
        switch (pos) {
            case LOAD_USERMAIN:
                loadUserMain();
                break;
            case LOAD_CREATE_TASK:
                ratingTask = null;
                loadCreateTask();
                break;
            case LOAD_CURRENT_PROJECT:
                loadProjectTab(selectedProjectPos);
                break;
        }
        findProjects();
    }

    /**
     * Loads {@link UserMainActivity}
     */
    private void loadUserMain() {
        toolbar.setTitle(R.string.userMainLabel);
        isCreatingTask = false;
        drawer.closeDrawers();
        selectedProjectPos = -1;
        userMain = new UserMainActivity();
        fragmentManager.beginTransaction().replace(CONTAINER, userMain).commit();
    }

    /**
     * Loads {@link ProjectTabActivity} and sets selectedProjectPos
     *
     * @param position in project list
     */
    private void loadProjectTab(int position) {
        isCreatingTask = false;
        selectedProjectPos = position;
        toolbar.setTitle(getProject().getProjectName());
        projectTabActivity = new ProjectTabActivity();
        fragmentManager.beginTransaction().replace(CONTAINER, projectTabActivity, String.valueOf(position)).commit();
    }

    /**
     * Loads create a task activity {@link CreateTaskActivity}
     */
    private void loadCreateTask() {
        isCreatingTask = true;
        toolbar.setTitle(R.string.create_task_label);
        fragmentManager.beginTransaction().replace(CONTAINER, new CreateTaskActivity()).commit();
    }

    /**
     * Loads create a project activity {@link CreateProjectActivity}
     */
    private void loadCreateProject() {
        toolbar.setTitle(R.string.create_a_project);
        fragmentManager.beginTransaction().replace(CONTAINER, new CreateProjectActivity()).commit();
    }

    public Project getProject() {
        return mProjectList.get(selectedProjectPos);
    }

    /**
     * Logout the current user and load login activity
     */
    private void logout() {
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                startActivity(new Intent(MainActivity.this, DispatchActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
    }

    /**
     * Query Parse for projects, and adds them to navDrawer
     * and updates projects List
     */
    private void findProjects() {
        navProgress.setVisibility(View.VISIBLE);
        ParseQuery<Project> pQuery = Project.getQueryWithRoleUsers();
        pQuery.findInBackground(new FindCallback<Project>() {
            @Override
            public void done(List<Project> list, ParseException e) {
                if (e == null) {
                    navProgress.setVisibility(View.INVISIBLE);
                    projectMenu.removeGroup(1);
                    mProjectList = list;
                    for (int i = 0; i < mProjectList.size(); i++)
                        projectMenu.add(1, i, 2, mProjectList.get(i).getProjectName());
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            ParUser myUser = (ParUser) ParseUser.getCurrentUser();
            Uri uri = data.getData();
            Bitmap bitmap = null;

            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (bitmap != null) {
                navImage.setImageBitmap(bitmap);
                userMain.updateCurrentUserFragmentBitmap(bitmap);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

                byte[] image = stream.toByteArray();
                ParseFile file = new ParseFile("myimg.png", image);
                myUser.putProfileImage(file);
                myUser.saveInBackground();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (selectedProjectPos >= 0) {
            if (isCreatingTask) {
                loadProjectTab(selectedProjectPos);
            } else {
                loadUserMain();
            }
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void ratedTask(int pos) {
        projectTabActivity.refreshTasks();
    }

    @Override
    public void onFragmentInteraction(Project project) {
        for (int i = 0; i < mProjectList.size(); i++)
            if (mProjectList.get(i).getObjectId().equals(project.getObjectId()))
                loadProjectTab(i);
    }

    @Override
    public void editTask(Task task) {
        ratingTask = task;
        loadCreateTask();
    }

    /**
     * @return a currentTask selected for editing
     */
    public Task getTask() {
        return ratingTask;
    }
}