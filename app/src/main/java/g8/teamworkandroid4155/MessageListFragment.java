package g8.teamworkandroid4155;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import g8.teamworkandroid4155.parseobjects.Message;
import g8.teamworkandroid4155.parseobjects.Project;

/**
 * Description of MessageListFragment.java
 *
 * @author Wesley Hager
 */
public class MessageListFragment extends Fragment {

    private ProgressDialog prog;
    private Context mContext;
    private ArrayAdapter adapter;
    private Button postButton;
    private EditText messageContent;
    private ListView messageListView;
    private List<Message> messageList;
    private Project mProject;


    public MessageListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mContext = getContext();
        mProject = ((MainActivity) mContext).getProject();

        prog = new ProgressDialog(this.mContext);
        prog.setTitle("Loading Messages");
        prog.setCancelable(false);

        View view = inflater.inflate(R.layout.fragment_message_list, container, false);

        postButton = (Button) view.findViewById(R.id.post_Button_Message);
        messageListView = (ListView) view.findViewById(R.id.message_List_Message);
        messageContent = (EditText) view.findViewById(R.id.content_Message);

        messageList = new ArrayList<>();

        adapter = new MessageListAdapter(mContext, R.layout.cell_message, messageList);
        messageListView.setAdapter(adapter);

        findMessages();

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewMessage();
            }
        });

        return view;
    }

    public void findMessages() {
        ParseQuery<Message> mQuery = ParseQuery.getQuery("Message");
        mQuery.whereEqualTo("projectPointer", mProject);
        mQuery.include("postBy");
        mQuery.orderByAscending("createdAt");
        mQuery.setLimit(1000);
        mQuery.findInBackground(new FindCallback<Message>() {
            @Override
            public void done(List<Message> list, ParseException e) {
                if (e == null) {
                    loadList(list);
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    private void loadList(List<Message> msg) {
        messageList.clear();
        messageList.addAll(msg);
        adapter.notifyDataSetChanged();
        scrollToBottom();
    }

    private void addMSGtoList(Message msg) {
        messageList.add(msg);
        adapter.notifyDataSetChanged();
        scrollToBottom();
    }

    private void scrollToBottom() {
        messageListView.setSelection(messageList.size() - 1);
    }

    private void createNewMessage() {
        String content = messageContent.getText().toString();

        if (!content.equals("")) {
            Message msg = new Message();
            msg.setMessageContent(content);
            msg.setMessagePostedBy(ParseUser.getCurrentUser());
            msg.setProject(mProject);
            msg.setACL(mProject.getACL());

            msg.saveInBackground();

            addMSGtoList(msg);

            messageContent.getText().clear();
        } else {
            Toast.makeText(mContext, "Message cannot be blank", Toast.LENGTH_SHORT).show();
        }
    }

}
