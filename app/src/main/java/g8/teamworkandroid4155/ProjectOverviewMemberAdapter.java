package g8.teamworkandroid4155;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;

import java.util.List;

import g8.teamworkandroid4155.parseobjects.ParUser;
import g8.teamworkandroid4155.parseobjects.Project;

/**
 * Description of ProjectOverviewMemberAdapter.java
 *
 * @author Wesley Hager
 */
public class ProjectOverviewMemberAdapter extends ArrayAdapter<ParseUser> {
    private List<ParseUser> userList;
    private Context mContext;
    private int mResource;
    private boolean projectDone;
    private Project mProject;
    private boolean allowRatings = true;

    public ProjectOverviewMemberAdapter(Context context, int resource, List<ParseUser> objects, Project mProject) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        this.userList = objects;
        this.mProject = mProject;
        projectDone = this.mProject.getProjectDone();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);
        }

        final ParUser user = (ParUser) userList.get(position);

        TextView username = (TextView) convertView.findViewById(R.id.user_name_card);
        final ImageView userImage = (ImageView) convertView.findViewById(R.id.profile_img_navBar);
        RatingBar comm = (RatingBar) convertView.findViewById(R.id.comm_RB_userCell);
        RatingBar help = (RatingBar) convertView.findViewById(R.id.help_RB_userCell);
        RatingBar part = (RatingBar) convertView.findViewById(R.id.part_RB_userCell);

        username.setText(user.getUsername());

        if (allowRatings) {// Show Project Related Stats
            comm.setRating((float) mProject.getMemberStat(user, Project.COMMUNICATION_STAT));
            help.setRating((float) mProject.getMemberStat(user, Project.HELPFULNESS_STAT));
            part.setRating((float) mProject.getMemberStat(user, Project.PARTICIPATION_STAT));
        } else { // Show Global stats in the search menu
            comm.setRating((float) user.getCommunicationStat());
            help.setRating((float) user.getHelpfulnessStat());
            part.setRating((float) user.getParticipationStat());
        }

        Button rate = (Button) convertView.findViewById(R.id.rateUser_btn_userCell);
        if (!user.getObjectId().equals(ParseUser.getCurrentUser().getObjectId()) && allowRatings) {

            rate.setVisibility(View.VISIBLE);
            rate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, ProjectReviewActivity.class).putExtra(Application.OTHER_USER, user.getObjectId()).putExtra(Application.CURRENT_PROJECT_ID, mProject.getObjectId()));
                }
            });
        } else {
            rate.setVisibility(View.GONE);

        }

        ParseFile file = user.getParseFile("picture");

        if (file != null) {
            file.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    if (e == null) {
                        Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        userImage.setImageBitmap(bmp);
                    } else {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            userImage.setImageResource(R.drawable.personimage);
        }


        return convertView;
    }

    public void setAllowRatings(boolean allowRatings) {
        this.allowRatings = allowRatings;
    }
}